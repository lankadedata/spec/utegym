# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/sv/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Länk till RFC 4180
- Länk till RFC 1738

## [1.0.0] - 2021-09-24
### Added
- Validerat JSON-schema.
- Publicerat som specifikation på [https://www.dataportal.se/sv/specifications/utegym/1.0](https://www.dataportal.se/sv/specifications/utegym/1.0).
- Publicerat webbsida på [https://lankadedata.se/spec/utegym/](https://lankadedata.se/spec/utegym/).
- Lägger till exempel på datamängd i CSV-, ODS- och XLSX-format.

### Fixed
- Korrigerat stav- och grammatikfel.
- Buggfix på CSV-exempel.
- Fixar fel i titel för kommunkod i schema.json.
- Namnfix på exempel.

## [0.5.0] - 2021-08-18
### Added
- JSON spec 
- Beslut om att lägga till attribut/kolumner för ej mappade träningsstationer (rygglyft, bänkpress etc). 
- CSV schema 
- Ytterligare dialog med wikidata 
- Beslut om att lägga till attributen "sign", “box”  och "log_lifting" 
- Flytta upp visit_url 
- belysning 

### Fixed
- sourcecode to git
- markdown-filer fixade 

## [0.2.9] - 2021-07-08
### Added
- markdown-filer

### Changed
- modellbeskrivning
- ordning av attribut

## [0.2.7] - 2021-07-07
### Changed
- e-post ändrad i beskrivning 
- lykta till lampa 

### Fixed

## [0.2.5] - 2021-07-02
### Added
- box, sign, log_lifting added
- examples  

### Changed
- lantern

## [0.2.0] - 2021-06-22
### Changed
“fitness_station=” borttaget ur kolumnnamn 
förtydligande av datatyper ändrat från tabell till paragrafer

### Fixed
Appendix A och B klart med exempel 

## [0.1.7] - 2021-06-21
### Changed
- Datatyp ändrat till boolean

### Fixed
- Appendix D städat och förkortade beskrivningar i modellen 

## [0.1.5] - 2021-06-17
### Added
- Alla attribut från OSM är tillagda, de attribut som inte finns nämnda ska skapas 
- Wikidata (Q-kod tillagt) 
- Wikidata query tillagt 
- 1-9 klart 
- Import från samarbetsdokument till GitLab.

[Unreleased]: 
