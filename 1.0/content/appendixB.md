# Exempel i JSON

Samma utegym som i Appendix A, fast nu som JSON.

```json
{
    "source": "1401",
    "id":  "0180-FinnsjoUtegym",
    "name":  "Finnsjö utomhusgym",
    "latitude":  "57.640382",
    "longitude":  "12.136265",
    "email":  "fritid@harryda.se",
    "visit_url":  "https://www.harryda.se/upplevaochgora/idrottmotionochfriluftsliv/motionochfriluftsliv/utegym",
    "wikidata":  "Q107213274",
    "updated":  "2021-06-22",
    "description":  "Finnsjös utegym består av 9 redskap vardera med möjlighet till mångsidig och varierad träning. Beläget nära Finnsjögården.",
    "street":  "",
    "housenumber":  "",
    "postcode":  "",
    "city":  "Härryda",
    "country":  "SE",
    "lighting":  true,
    "sit-up":  false,
    "push-up":  false,
    "paralell_bars":  false,
    "horizontal_ladder":  false,
    "balance_beam":  false,
    "horizontal_bar":  true,
    "battling_ropes":  false,
    "captains_chair":  false,
    "slackline":  false,
    "rower":  false,
    "air_walker":  false,
    "exercise_bike":  true,
    "rings":  true,
    "elliptical_trainer":  true,
    "hurdling":  true,
    "log_lifting":  true,
    "box":  true,
    "sign":  true,
    "td_url":  "https://www.t-d.se/sv/TD2/Avtal/UDDEVALLA-KOMMUN-/Halligangparken-/Utegym-i-Halligangparken-/"
}
```

<div class="note">
I JSON formatet kan tomma värden representeras på flera sätt, följande sätt bör accepteras:

* som tomma strängen, det ser vi exemplet ovan som "".
* med hjälp av det speciella värdet `null`, observera att det inte ska skrivas med omgivande citattecken.
* genom att helt utelämna fält.
</div>