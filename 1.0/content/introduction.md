# Introduktion
_Med utegym menas en särskilt anlagd allmän plats där träning kan ske. I texten nedan används “utegym” för att benämna detta._

Denna specifikation definierar en enkel tabulär informationsmodell för utegym. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formatet CSV och JSON. Som grund förutsätts CSV kunna levereras då det är praktiskt format och skapar förutsägbarhet för mottagare av informationen. Det är också ett format som gör sammanställningen av datamängden enklare då den kan representeras i t.ex. ett kalkylblad.